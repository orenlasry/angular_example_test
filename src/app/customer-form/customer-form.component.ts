import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'customerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {
  @Input() Lable:string;
  @Input() buttonLable:string;
  @Input() name:string;
  @Input() income:number;
  @Input() id:string;
  @Input() education:number;
  @Output() update = new EventEmitter<Customer>();
  @Output() closeEdit = new EventEmitter<null>();

  buttonText:String = 'Add customer'; 
  

  updateParent(){
    let customer:Customer = {
      id:this.id,
      name:this.name,
      education:this.education,
      income:this.income
    };
    this.update.emit(customer);
    if(this.Lable == 'Add Customer'){
      this.name = null;
      this.education = null;
      this.income = null;
    }
  }

  tellParentToClose(){
    this.closeEdit.emit();
  }

  constructor() { }

  ngOnInit(): void {
  }

}
