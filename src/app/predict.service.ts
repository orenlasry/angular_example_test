import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ReturnStatement } from '@angular/compiler';


@Injectable({
  providedIn: 'root'
})
export class PredictService {
  private url = "https://gs43tk15hk.execute-api.us-east-1.amazonaws.com/beta";

  predict(education:number, income:number){
    let json= {
      'data':{
        "education":education,
        "income":income
      }
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url, body).pipe(
      map(res=>{
        console.log(res);
        let final = res.body;
        console.log(final);
        final = final.replace('[','')
        final = final.replace(']','')
        return final
      })
    )



  }

  constructor(private http:HttpClient) { }
}
