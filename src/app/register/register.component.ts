import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  hide = true;
  email:string;
  password:string;
  errorMessage:string;
  errorCode:string;

  constructor(private auth:AuthService, private router:Router) { }

  onSubmit(){
    this.auth.register(this.email, this.password).then(res =>{
      console.log(res);
      this.router.navigate(['/wellcome']);
        
    }
    ).catch(err =>
    {
      // Handle Errors here.
      this.errorCode = err.code;
      this.errorMessage = err.message;
      console.log(err);
    })


  }





  ngOnInit(): void {
  }

}
