import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  hide = true;
  errorMessage:string;
  errorCode:string;
  email:string;
  password:string;

  userEmail:string;

  onSubmit(){
      this.auth.login(this.email, this.password).then(res =>{
        console.log(res);
        this.router.navigate(['/wellcome']);
      }
      ).catch(err =>
      {
        // Handle Errors here.
        this.errorCode = err.code;
        this.errorMessage = err.message;
        console.log(err);
      });



  }

  constructor(public auth:AuthService,private router:Router) { }

  ngOnInit(): void {
  }

}
