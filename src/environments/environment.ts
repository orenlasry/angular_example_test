// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyA1x-kbVIfTU2Gz8tUESX0Sib0oYp0MHPQ",
    authDomain: "example-test-2021.firebaseapp.com",
    projectId: "example-test-2021",
    storageBucket: "example-test-2021.appspot.com",
    messagingSenderId: "854856300745",
    appId: "1:854856300745:web:e96a9df3c726fe1e21b518",
    measurementId: "G-LD030NP9MK"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
